package org.nuclos.api.ui;

import java.util.Set;

import javax.swing.Icon;

import org.nuclos.api.UID;

public interface ResultToolbarItemFactory {

	/**
	* Returns the id
	* @return
	* the id
	*/
	public Object getId();
	
	/**
	 * Returns the UID of the business object for the result list the button is shown in.
	 * If the return value is null the button is shown in every result list of all business objects 
	 * @return the UID of the business object
	 */
	public UID getEntityUid();
	
	/**
	 * Returns the button's label.
	 * @return the button's label
	 */
	public String getLabel();
	
	/**
	 * Returns the button's tooltip.
	 * @return the button's tooltip
	 */
	public String getTooltip();
	
	/**
	 * Returns the button's icon which is placed left of the label.
	 * @return the button's icon
	 */
	public Icon getIcon();
	
	/**
	 * Returns an integer which is used to determine the order of the buttons in the result list toolbar.
	 * If two button share the same order number the order depends on which class is found first by the class loader which is unpredictable.
	 * @return order number
	 */
	public int getOrder();
	
	/**
	 * Returns true if the user is allowed to use this button based on actions in T_AD_ACTION granted to him/her.
	 * @param grantedActions set of UIDs from actions defined in T_AD_ACTION granted to the current user via "administration/user roles"
	 * @return true if user is allowed to use this button
	 */
	public boolean isAllowed(Set<UID> grantedActions);
	
	/**
	 * Resturns new button.
	 * @return new button
	 */
	public ResultToolbarItem newInstance();
}
