//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ui;

import javax.swing.Icon;
import javax.swing.JComponent;

import org.nuclos.api.Settings;

public interface UserSettingsEditor {
	
	/**
	 * 
	 * @return name for User Settings Dialog
	 */
	String getName();

	/**
	 * 
	 * @return icon for User Settings Dialog
	 */
	Icon getIcon();
	
	/**
	 * 
	 * @return
	 */
	String getSettingsKey();
	
	/**
	 * @param prefs Settings for the current user
	 * @return Component for User Settings Dialog
	 */
	JComponent getSettingsComponent(Settings prefs);
	
	/**
	 * called on save
	 * @return Settings for the current user
	 */
	Settings getSettings();
	
	/**
	 * 
	 * @param saved
	 */
	void close(boolean saved);

}
