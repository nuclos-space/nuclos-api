package org.nuclos.api.ui;

import java.io.Serializable;
import java.util.Map;

public interface InputContextSupport {

	public void setup(Map<String, Serializable> data);
	
	public Map<String, Serializable> evaluate();
}
