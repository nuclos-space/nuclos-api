package org.nuclos.api.service;

import java.util.List;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;

public interface MailService {

		void send(NuclosMail mail) throws BusinessException;
		List<NuclosMail> receive(boolean bDeleteMails) throws BusinessException;
		List<NuclosMail> receive(String folderFrom, boolean bDeleteMails) throws BusinessException;
		
}
