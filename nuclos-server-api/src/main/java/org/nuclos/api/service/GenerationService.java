//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.generation.Generation;

public interface GenerationService {

	public <S extends BusinessObject, T extends BusinessObject> T 
		execute(S s, Class<? extends Generation<S,T>> genClass) throws BusinessException;
	
	public <S extends BusinessObject, T extends BusinessObject> List<T> 
		execute(List<S> s, Class<? extends Generation<S,T>> genClass) throws BusinessException;

	
}
