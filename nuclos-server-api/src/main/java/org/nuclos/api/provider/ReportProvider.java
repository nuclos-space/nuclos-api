//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.report.Report;
import org.nuclos.api.service.ReportService;

/**
 * {@link ReportProvider} provides methods for executing reports
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Matthias Reichart
 */
public class ReportProvider {

	private static ReportService service;
	
	public void setReportService(ReportService repService) {
		this.service = repService;
	}
	
	private static ReportService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * This method runs the {@link Report} that belongs to the given {@link OutputFormat} and returns 
	 * the generated {@link NuclosFile} 
	 * 
	 * @param format {@link OutputFormat} - ReportOutputFormat stored in the generated report class
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format) 
			throws BusinessException {
		return getService().run(format);
	}
	

	/**
	 * Providing the parameters this method runs the {@link Report} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile}
	 * 
	 * @param format {@link OutputFormat} - ReportOutputFormat stored in the generated report class
	 * @param params {@link Map} - Parameters for the report
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Map<String, Object> params) 
			throws BusinessException {
		return getService().run(format, params);
	}
	
	/**
	 * This method runs the {@link Report} that belongs to the given {@link OutputFormat} and returns 
	 * the generated {@link NuclosFile} containing all localized data in the given locale language 
	 * 
	 * @param format {@link OutputFormat} - ReportOutputFormat stored in the generated report class
	 * @param locale {@link NuclosLocale} - Data locale
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, NuclosLocale locale) 
			throws BusinessException {
		return getService().run(format, locale);
	}
	

	/**
	 * Providing the parameters this method runs the {@link Report} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile} containing all localized data in the given locale language
	 * 
	 * @param format {@link OutputFormat} - ReportOutputFormat stored in the generated report class
	 * @param params {@link Map} - Parameters for the report
	 * @param locale {@link NuclosLocale} - Data locale
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Map<String, Object> params, NuclosLocale locale) 
			throws BusinessException {
		return getService().run(format, params, locale);
	}
	
	public static NuclosFile run(UID outputFormatId, Map<String, Object> params, NuclosLocale locale) 
			throws BusinessException {
		return getService().run(outputFormatId, params, locale);
	}
}
