package org.nuclos.api.provider;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.MandatorService;
import org.nuclos.api.service.UserService;

import java.util.Date;

/**
 * The {@link MandatorProvider} provides several methods to create mandators and manipulate mandator role data
 *
 */
public class MandatorProvider {

	private static MandatorService service;

	public void setMandatorService(MandatorService repService) {
		this.service = repService;
	}

	private static MandatorService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}

	/**
	 * This method creates a Mandator and returns the id of the new object.<br>
	 *
	 * @param name
	 * @return UID
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(String name ) throws BusinessException {
		return getService().insert(name);
	}

	/**
	 * This method creates a Mandator and returns the id of the new object.<br>
	 *
	 * @param name
	 * @return UID
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(String name, NuclosMandator parentMandator) throws BusinessException {
		return getService().insert(name,parentMandator);
	}
}
