//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.communication;

import org.nuclos.api.context.communication.RequestContext;

public interface CommunicationInterface {
	
	/**
	 * Use the registration to describe your interface. Nuclos calls this method on server start.
	 * All information are optional.
	 * 
	 * @param registration
	 */
	void register(Registration registration);
	
	public interface Registration {
		
		/**
		 * if not set, default is false
		 * @param isMultiInstanceable
		 */
		void setMultiInstanceable(boolean isMultiInstanceable);
		
		/**
		 * @param description
		 */
		void setDescription(String description);
		
		/**
		 * @param helpUrl
		 */
		void setHelpUrl(String helpUrl);
		
		/**
		 * @param name
		 * @param description
		 */
		void addSystemParameter(String name, String description);

		/**
		 * @param requestContextClass
		 */
		<C extends RequestContext<?>> void addSupportedRequestContext(Class<C> requestContextClass);
		
	}

}
