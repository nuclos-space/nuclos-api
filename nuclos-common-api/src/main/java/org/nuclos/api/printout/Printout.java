//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.printout;

import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.report.OutputFormat;

/**
 * Class representing a printout object used in rule programming
 * @author reichama
 *
 */
public interface Printout {
	
	/**
	 * returns the id of the printout object
	 * @return
	 */
	UID getId();
	
	/**
	 * set id of the business object
	 * 
	 * @param boId business object id
	 */
	public void setBusinessObjectId(Long boId);
	
	/**
	 * returns the id of the business object
	 * 
	 * @return id of the business object
	 */
	public Long getBusinessObjectId();
	
	/**
	 * returns assigned {@link OutputFormat} 
	 * 	 
	 * @return list of {@link OutputFormat} 
	 */
	public <T extends OutputFormat> List<T> getOutputFormats();
}
