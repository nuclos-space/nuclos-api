//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject.attribute;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;

/**
 * Represents an Attribute of a {@link BusinessObject}.
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class Attribute<T> {

	private final String constantName;
	private final String packageName;
	private final String entityUid;
	private final String attributeUid;
	private final Class<T> type;
	
	public Attribute(String pConstantName, String pPackageName, String pEntityUid, 
			String pAttributeUid, Class<T> pType) {
		this.constantName = pConstantName;
		this.packageName = pPackageName;
		this.entityUid = pEntityUid;
		this.attributeUid = pAttributeUid;
		this.type = pType;
	}
	
	/**
	 * This method enables the user to compare an attribute with a given value on being equal
	 * @param value
	 * @return
	 */
	public SearchExpression<T> eq(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.EQUALS);
	}
	
	/**
	 * This method enables the user to compare an attribute with a given value on being unequal
	 * @param value
	 * @return
	 */
	public SearchExpression<T> neq(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.UNEQUALS);
	}
	
	/**
	 * This method enables the user to ensure that a value is not null
	 */
	public SearchExpression<T> notNull() {
		return new SearchExpression<T>(this, QueryOperation.NOTNULL);
	}
	
	/**
	 * This method enables the user to ensure that a value is null
	 */
	public SearchExpression<T> isNull() {
		return new SearchExpression<T>(this, QueryOperation.NULL);
	}

	/**
	 * for internal use only
	 */
	public String getConstantName() {
		return constantName;
	}
	
	/**
	 * for internal use only
	 */
	public String getPackageName() {
		return packageName;
	}
	
	/**
	 * for internal use only
	 */
	public String getAttributeUid() {
		return attributeUid;
	}
	
	/**
	 * for internal use only
	 */
	public String getEntityUid() {
		return entityUid;
	}
	
	/**
	 * for internal use only
	 */
	public Class<T> getType() {
		return type;
	}
	
}
