package org.nuclos.api.businessobject.attribute;

import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;

/**
 * {@link StringAttribute} is used if the attribute type provides comparison operations based on Strings
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class StringAttribute<T> extends Attribute<T> {

	public StringAttribute(String pConstantName, String pPackageName,
			String pEntityUid, String pAttributeUid, Class<T> pType) {
		super(pConstantName, pPackageName, pEntityUid, 
		  	  pAttributeUid, pType);
	}

	/**
	 * Like - comparison operation
	 * @param value
	 * @return
	 */
	public SearchExpression<T> like(T value) {
		return new SearchExpression<T>(this, value, QueryOperation.LIKE);
	}
		
}
