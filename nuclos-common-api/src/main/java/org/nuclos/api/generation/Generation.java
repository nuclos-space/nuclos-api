package org.nuclos.api.generation;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link Generation} is the main interface of all Nuclos generations. Nuclos generations are generated classes which type-safety
 * determines the source and the target objects. 
 * 
 * @see BusinessException
 * @author Matthias Reichart
 */
public interface Generation<S extends BusinessObject,T extends BusinessObject> {

	UID getId();
	
	Class<S> getSourceModule();
	
	Class<T> getTargetModule();

}
