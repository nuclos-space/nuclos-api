//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api;

import java.io.Serializable;

public class Property implements Serializable {

	private static final long serialVersionUID = 192123756791235L;
	
	public final String name;
	public final Class<?> type;
	
	public Property(String name, Class<?> type) {
		super();
		if (name == null) {
			throw new IllegalArgumentException("Name must not be null");
		}
		if (type == null) {
			throw new IllegalArgumentException("Type must not be null");
		}
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Property) {
			Property otherProperty = (Property) obj;
			return ((type == null) ? (otherProperty.type == null) : type.equals(otherProperty.type)) && 
					((name == null) ? (otherProperty.name == null) : name.equals(otherProperty.name));
		}
		return super.equals(obj);
	}
	@Override
	public int hashCode() {
		return name==null? super.hashCode() : name.hashCode();
	}
	@Override
	public String toString() {
		return "Property[name=" + name + ",type=" + type + "]";
	}
	
}
