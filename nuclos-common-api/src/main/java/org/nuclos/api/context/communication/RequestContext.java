//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context.communication;

import org.nuclos.api.communication.response.RequestResponse;

public interface RequestContext<R extends RequestResponse> extends CommunicationContext {

	/**
	 * @return
	 * 		overrides old response with a new one
	 */
	public R clearResponse();
	
	/**
	 * if no response is present clearResponse is called automatically
	 * 
	 * @return
	 * 		the response
	 */
	public R getResponse();
	
	/**
	 * internal use only
	 * @return 
	 * 		true, if a response object is created via {@link #clearResponse()} or {@link #getResponse()} before
	 */
	boolean isResponse();

}
