//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.util.Locale;

import org.nuclos.api.User;


/**
 * {@link RuleContext} represents the base context that supports elementary functions like
 * caching that enables to pass objects throw a list of rules that are executed during one event.
 *
 * @author Matthias Reichart
 */
public interface RuleContext extends LogContext {

	/**
	 * This method returns the cached object that can be found for the given key
	 * 
	 * @param key
	 * @return CacheObject
	 */
	public Object getContextCacheValue(String key);
	
	/**
	 * This methods adds a Objects to the cache using the given key
	 * @param key
	 * @param value
	 */
	public void addContextCacheValue(String key, Object value);
	
	/**
	 * This method removes all cached data that can be found for the given key
	 * @param key
	 */
	public void removeContextCacheValue(String key);

	/**
	 * Returns the current user
	 * @return
	 */
	public User getUser();
	
	/**
	 * Returns the {@link Locale} of current user
	 * @return
	 */
	public String getLanguage();
	
}
