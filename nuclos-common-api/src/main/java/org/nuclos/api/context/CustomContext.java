//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;

/**
 * {@link CustomContext} represents the context used in a Custom-Rule
 * <p>It contains the {@link BusinessObject} to edit and several functions like caching
 *
 * @see RuleContext
 * @author Matthias Reichart
 */
public interface CustomContext extends RuleContext {


	/**
	 * This method returns the {@link BusinessObject} containing the entry-data.
	 * 
	 * @param t Classtype being a {@link BusinessObject}
	 * @return {@link BusinessObject}
	 */
	public <T extends BusinessObject> T getBusinessObject(Class<T> t);

	/**
	 * This method returns the {@link GenericBusinessObject} containing the entry-data to edit
	 * during runtime.
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;

	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 * @param prio
	 */
	public void notify(String message, Priority prio);
	
	/**
	 * Creates a message with normal {@link Priority}. It is displayed
	 * in the notification dialog in nuclos as a message. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 */
	public void notify(String message);
	
	/**
	 * Sets flag "UpdateAfterExecution" to control if the {@link BusinessObject} of the context is updated
	 * after rule execution. "true" means: update after rule execution.
	 * @param bUpdateAfterExecution
	 */
	public void setUpdateAfterExecution(boolean bUpdateAfterExecution);
	
	/**
	 * This method returns true if the {@link BusinessObject} of the context is updated
	 * after rule execution.
	 * @return true or false
	 */
	public boolean getUpdateAfterExecution();
	
}
