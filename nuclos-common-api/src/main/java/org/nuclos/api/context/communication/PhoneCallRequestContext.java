//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context.communication;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.api.communication.response.PhoneCallResponse;

public interface PhoneCallRequestContext extends RequestContext<PhoneCallResponse>, InstantiableContext {
	
	/**
	 * 
	 * @param toNumber
	 */
	public void setToNumber(String toNumber);
	
	/**
	 * 
	 * @param userAccount
	 */
	void setNuclosUserAccount(NuclosUserCommunicationAccount userAccount);
	
	/**
	 * 
	 * @param businessObject
	 */
	<T extends BusinessObject<?>> void setBusinessObject(T businessObject);
	
	/**
	 * 
	 * @return
	 * 		the called phone number
	 */
	public String getToNumber();

	/**
	 * 
	 * @return
	 * 		the calling user (account) 
	 */
	public NuclosUserCommunicationAccount getNuclosUserAccount();
	
	/**
	 * 
	 * @return
	 * 		the business object the number belongs to
	 */
	public <T extends BusinessObject<?>> T getBusinessObject(Class<T> t);

}
