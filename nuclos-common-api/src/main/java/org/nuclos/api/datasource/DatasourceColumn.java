//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.datasource;

/**
 * Represents a column of a {@link DatasourceResult} received by 
 * DatasourceProvider after executing a
 * datasource-query
 * 
 * @author Matthias Reichart
 */
public interface DatasourceColumn {

	/**
	 * Returns the name of the column
	 * @return String
	 */
	String getName();
	
	
	/**
	 * Returns the type of the column as class
	 * @return String
	 */
	Class<?> getType();
	
}
