//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.common;

import java.util.Date;

import org.nuclos.api.UID;

public interface NuclosUser {

	UID getId();

	String getUsername();
	void setUsername(String username);

	String getFirstname();
	void setFirstname(String firstname);

	String getLastname();
	void setLastname(String lastname);

	String getEmail();
	void setEmail(String email);

	Boolean getLoginWithEmailAllowed();
	void setLoginWithEmailAllowed(Boolean loginWithEmailAllowed);

	Boolean getPasswordChangeRequired();
	void setPasswordChangeRequired(Boolean passwordChangeRequired);

	String getPasswordResetCode();
	void setPasswordResetCode(String passwordResetCode);

	Boolean getSuperuser();
	void setSuperuser(Boolean superuser);

	Boolean getLocked();
	void setLocked(Boolean locked);

	Date getExpirationDate();
	void setExpirationDate(Date expirationDate);

	UID getCommunicationAccountPhoneId();

	String getActivationCode();
	void setActivationCode(String activationCode);

	Boolean getPrivacyConsentAccepted();
	void setPrivacyConsentAccepted(Boolean privacyConsentAccepted);

	Date getLastPasswordChange();

	Date getLastLogin();

	Integer getLoginAttempts();

}
