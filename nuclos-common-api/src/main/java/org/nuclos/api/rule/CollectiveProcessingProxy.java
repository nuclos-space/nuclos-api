package org.nuclos.api.rule;

import java.util.Collection;
import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;

public interface CollectiveProcessingProxy {
	
   <PK> List<? extends BusinessObject<PK>> getForCollectiveProcessing(ForeignKeyAttribute<PK> attribute, Collection<PK> ids);

}
