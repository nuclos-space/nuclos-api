package org.nuclos.api.ide.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WebAddon Utilities, also used by the nuclos-source-maven-plugin
 *
 * @author Maik Stueker
 * @since Nuclos 4.36
 */
public class WebAddonUtils {

	public static final Logger LOG = LoggerFactory.getLogger(WebAddonUtils.class);

	/**
	 * convert camel case for file name (e.g. "TestAddon" converted to "test-addon")
	 *
	 * @param s
	 * @return formatted String
	 */
	public static String camelCaseToFileName(final String s) {
		return s.replaceAll(
				String.format("%s|%s|%s",
						"(?<=[A-Z])(?=[A-Z][a-z])",
						"(?<=[^A-Z])(?=[A-Z])",
						"(?<=[A-Za-z])(?=[^A-Za-z])"
				),
				"-"
		).toLowerCase();
	}

	/**
	 * generate addon.modules.ts which contains the registered addon modules
	 */
	public static boolean generateWebAddonModulesRegistry(Set<String> webAddons, boolean addonDevMode, Path path) {

		if (!path.getParent().toFile().exists()) {
			path.getParent().toFile().mkdirs();
		}

		final StringBuilder addonModulesRegistry = new StringBuilder();
		addonModulesRegistry.append("/* generated file */\n\n");
		addonModulesRegistry.append("import { NgModule } from '@angular/core';\n\n");
		for (String webAddon : webAddons) {
			String fileName = WebAddonUtils.camelCaseToFileName(webAddon);
			if (addonDevMode) {
				fileName = "../addons/" + fileName + "/src";
			}
			addonModulesRegistry.append("import { " + webAddon + "Module } from '" + fileName + "';\n");
		}

		addonModulesRegistry.append("\n@NgModule({\n");
		addonModulesRegistry.append("\timports: [\n");
		for (String webAddon : webAddons) {
			addonModulesRegistry.append("\t\t" + webAddon + "Module,\n");
		}
		addonModulesRegistry.append("\t]\n");
		addonModulesRegistry.append("})\n");
		addonModulesRegistry.append("export class AddonModules {\n");
		addonModulesRegistry.append("}\n");

		LOG.info(addonModulesRegistry.toString());

		try {
			String oldFileContent = Files.exists(path) ? new String(Files.readAllBytes(path)) : null;
			final String addonModulesString = addonModulesRegistry.toString();

			// write only if file differs
			if (!addonModulesString.equals(oldFileContent)) {
				Path parentDir = path.getParent();
				if (!Files.exists(parentDir)) {
					Files.createDirectories(parentDir);
				}
				Files.write(path, addonModulesString.getBytes("UTF-8"));
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, path, ex);
			return false;
		}

		return true;
	}

}
