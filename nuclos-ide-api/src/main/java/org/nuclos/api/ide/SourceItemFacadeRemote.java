//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ide;

import java.net.URL;

import org.nuclos.api.ide.valueobject.INuclosApi;
import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.SourceType;

/**
 * Remote facade for supporting java event support sources synchronization to the client.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.13
 */
public interface SourceItemFacadeRemote {
	
	/**
	 * Get the source items (beginning with root), containing all source items (of the given type) 
	 * that are defined in the nuclos instance.
	 * 
	 * @param type SourceType of SourceItem. By definition <code>null</code> means 'all types for SourceItems'.
	 * @param qualifiedPackagenameAsRoot Root node of the subtree of SourceItems to retrieve. By definition 
	 * 		<code>null</code> means 'complete tree, beginning at root'. (If qualifiedPackagenameAsRoot happens 
	 * 		to be a Classname, the result is always <code>null</code>.)
	 * @param includeOldApiRules true to include 'old rules'
	 * @return Root node with up-to-date subtree. (Normally the result is <em>not</em> a copy of the given
	 * 		parameter root.)
	 * @throws IllegalAccessException 
	 */
	ISourceItem getTree(SourceType type, String qualifiedPackagenameAsRoot, boolean includeOldApiRules) throws IllegalAccessException;
	
	/**
	 * Get an URL that can be used for downloading the java source of an source item (business object or event source).
	 * @throws IllegalAccessException 
	 */
	URL getDownloadURL(String qualifiedClassname, boolean src) throws IllegalAccessException;

	/**
	 * Get info about the nuclos API version.
	 * 
	 * @return Nuclos API information
	 * @throws IllegalAccessException 
	 */
	INuclosApi getNuclosApi() throws IllegalAccessException;
	
	/**
	 * Get an URL that can be used for downloading the nuclos api jar file.
	 * 
	 * @throws IllegalAccessException 
	 */
	URL getNuclosApiDownloadURL(String jarFileName) throws IllegalAccessException;

	/**
	 * Upload the given java source item.
	 * 
	 * @param item
	 * @param isOldRule true if this rule is part of the 'old rule API'
	 * @param content
	 * @return The ISourceItem as saved on the server side. This is important as the SourceScanner will add a prefix
	 * 		comment to the saved source. If the this return value is different form the parameter <code>item</code> 
	 * 		given, the IDE <em>must</em> reload the source. It is recommended to reload after {@link #forceRuleCompilation()}.
	 * @throws IllegalAccessException 
	 */
	ISourceItem uploadSourceItem(ISourceItem item, boolean isOldRule, byte[] content) throws IllegalAccessException;
	

	/**
	 * Force a new rule compilation on the server.
	 * <p>
	 * This method is called after all (altered) java source items have been uploaded with 
	 * {@link #uploadSourceItem(ISourceItem, boolean, byte[])}.
	 * </p>
	 */
	void forceRuleCompilation();
	
}
